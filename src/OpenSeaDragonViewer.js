// import  { parseJSON } from "openseadragon";
import OpenSeadragon from "openseadragon";
import React, { useEffect, useState } from "react";
import * as Annotorious from "@recogito/annotorious-openseadragon";
import "@recogito/annotorious-openseadragon/dist/annotorious.min.css";

const OpenSeaDragonViewer = ({ image }) => {
  const [viewer, setViewer] = useState(null);
  const [anno, setAnno] = useState(null);
  const [annotations, setAnnotations] = useState([]);

  useEffect(() => {
    if (image && viewer) {
      localStorage.setItem("image", image.source.Image.Url);
      viewer.open(image.source);
      //   viewer.screenshot({
      //     showOptions: true, // Default is false
      //     keyboardShortcut: 'p', // Default is null
      //     showScreenshotControl: true // Default is true

      // });
    }
    // if (image && anno) {
    //   //  these
    //   InitAnnotations(); //  are the updated
    //   // viewer.initializeAnnotations();
    // } //  lines
  }, [image]);
  // // console.log('image1', image)

  const getLocalAnnotations = () => {
    return localStorage.getItem(image.source.Image.Url);
  };
  const setLocalAnnotation = (newAnnotations) => {
    localStorage.setItem(
      image.source.Image.Url,
      JSON.stringify(newAnnotations)
    );
  };

  const InitOpenseadragon = () => {
    viewer && viewer.destroy();
    // eslint-disable-next-line no-undef
    const initViewer = OpenSeadragon({
      id: "openseadragon1",
      prefixUrl: "openseadragon-images/",
      animationTime: 0.5,
      blendTime: 0.1,
      constrainDuringPan: true,
      minZoomImageRatio: 0.2,
      maxZoomPixelRatio: 5,
      minZoomLevel: 1,
      visibilityRatio: 1,
      zoomPerScroll: 2,
      showNavigator: true,
      pixelsPerMeter: 4,
      tileSources: {
        Image: {
          xmlns: "http://schemas.microsoft.com/deepzoom/2008",
          Url: "http://content.zoomhub.net/dzis/TDbz_files/",
          Format: "jpg",
          Overlap: "1",
          TileSize: "254",
          ServerFormat: "Default",
          Size: {
            Height: "4409",
            Width: "7793",
          },
        },
      },
    });

    setViewer(initViewer);
    const config = {};
    const annotate = Annotorious(initViewer, config);
    setAnno(annotate);
  };

  // const InitAnnotations = async () => {
  //   const storedAnnoatations = getLocalAnnotations;

  //   if (storedAnnoatations) {
  //     // const annotations = OpenSeadragon.parseJSON(storedAnnoatations);
  //     // eslint-disable-next-line no-undef
  //     const annotations = OpenSeadragon.Annotations({ viewer });
  //     console.log(annotations, "=========");
  //     setAnnotations(annotations);
  //     anno.setAnnotations(annotations);
  //   }
  //   console.log(anno, "=======");
  //   anno.on("createAnnotation", (annotation) => {
  //     const newAnnotations = [...annotations, annotation];
  //     setAnnotations(newAnnotations);
  //     setLocalAnnotation(newAnnotations);
  //   });
  //   anno.on("updateAnnotation", (annotation, previous) => {
  //     const newAnnotations = annotations.map((val) => {
  //       if (val.id === annotation.id) return annotation;
  //       return val;
  //     });
  //     setAnnotations(newAnnotations);
  //     setLocalAnnotation(newAnnotations);
  //   });
  //   anno.on("deleteAnnotation", (annotation) => {
  //     const newAnnotations = annotations.filter(
  //       (val) => val.id !== annotation.id
  //     );
  //     setAnnotations(newAnnotations);
  //     setLocalAnnotation(newAnnotations);
  //   });
  // };

  useEffect(() => {
    const script = document.createElement("script");

    script.src = "../openseadragon/openseadragon.min.js";
    script.async = true;

    document.body.appendChild(script);

    const script_anno = document.createElement("script");

    script_anno.src =
      "../openseadragon-annotations/openseadragon-annotations.js";
    script_anno.async = true;

    document.body.appendChild(script_anno);

    // eslint-disable-next-line no-undef

    InitOpenseadragon();
    // eslint-disable-next-line no-undef

    return () => {
      viewer && viewer.destroy();
      document.body.removeChild(script);
      document.body.removeChild(script_anno);
    };
  }, []);

  // useEffect(() => {
  //   var duomo = {
  //     Image: {
  //       xmlns: "http://schemas.microsoft.com/deepzoom/2008",
  //       Url: "http://content.zoomhub.net/dzis/TDbz_files/",
  //       Format: "jpg",
  //       Overlap: "2",
  //       TileSize: "256",
  //       Size: {
  //         Width: "13920",
  //         Height: "10200",
  //       },
  //     },
  //   };
  // var in_viewer = OpenSeadragon({
  //     id: "seadragon-viewer",
  //     prefixUrl: "openseadragon-images/",
  //     tileSources: duomo,
  //   });
  
  //   in_viewer.initializeAnnotations();
  //   setViewer(in_viewer);
  // }, [])

  return (
    <div
      id="openSeaDragon"
      style={{
        height: "800px",
        width: "1200px",
      }}
    ></div>
  );
};

export { OpenSeaDragonViewer };
